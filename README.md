### Yenjie Repository for [CRUX Linux](https://crux.nu/)

This repository contains ports for CRUX Linux. If you wish to use this repository choose from only one of the two files [yenjie.git](https://gitlab.com/SiFuh/yenjie/-/raw/master/yenjie.git) or [yenjie.httpup](https://gitlab.com/SiFuh/yenjie/-/raw/master/yenjie.httpup). __Do not use both__. Place one of these files in the config directory for ports **/etc/ports**. Then add the line **prtdir /usr/ports/yenjie** to **/etc/prt-get.conf**.

---
![Alt text](https://gitlab.com/SiFuh/Documentation/-/raw/master/yenjie.png)
