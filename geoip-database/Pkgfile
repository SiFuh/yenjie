# Description:  GeoIP legacy country database (based on GeoLite2 data created by MaxMind)
# URL:          https://mailfud.org/geoip-legacy/
# Maintainer:   Lin SiFuh, #crux at irc dot libera dot chat
# Depends on:   geoip

name=geoip-database
version=20211221
release=1
source=(https://sources.archlinux.org/other/packages/${name}/${version}/GeoIP.dat.gz
        https://sources.archlinux.org/other/packages/${name}/${version}/GeoIPv6.dat.gz
        https://sources.archlinux.org/other/packages/${name}/${version}/GeoIPCity.dat.gz
        https://sources.archlinux.org/other/packages/${name}/${version}/GeoIPCityv6.dat.gz
        https://sources.archlinux.org/other/packages/${name}/${version}/GeoIPASNum.dat.gz
        https://sources.archlinux.org/other/packages/${name}/${version}/GeoIPASNumv6.dat.gz)

build() {

  cd "${SRC}"
  gunzip *.gz

  if [[ $(geoiplookup -f GeoIP.dat 8.8.8.8) != *'US, United States' ]]; then
    echo >&2 'Unable to resolve IPv4 address to country.'
    return 1
  fi

  if [[ $(geoiplookup6 -f GeoIPv6.dat 2001:4860:4860::8888) != *'US, United States' ]]; then
    echo >&2 'Unable to resolve IPv6 address to country.'
    return 1
  fi

  if [[ $(geoiplookup -f GeoIPCity.dat 8.8.8.8) != *'US, 00, N/A, N/A'* ]]; then
    echo >&2 'Unable to resolve IPv4 address to city.'
    return 1
  fi

  if [[ $(geoiplookup6 -f GeoIPCityv6.dat 2001:4860:4860::8888) != *'US, 00, N/A, N/A'* ]]; then
    echo >&2 'Unable to resolve IPv6 address to city.'
    return 1
  fi

  if [[ $(geoiplookup -f GeoIPASNum.dat 8.8.8.8) != *'AS15169 GOOGLE' ]]; then
    echo >&2 'Unable to resolve IPv4 address to ASN.'
    return 1
  fi

  if [[ $(geoiplookup6 -f GeoIPASNumv6.dat 2001:4860:4860::8888) != *'AS15169 GOOGLE' ]]; then
    echo >&2 'Unable to resolve IPv6 address to ASN.'
    return 1
  fi



  install -d "${PKG}/usr/share/GeoIP"
  install -m644 -t "${PKG}/usr/share/GeoIP" GeoIP{,v6}.dat

  install -d "${PKG}/usr/share/GeoIP"
  install -m644 -t "${PKG}/usr/share/GeoIP" GeoIPCity{,v6}.dat GeoIPASNum{,v6}.dat

}
